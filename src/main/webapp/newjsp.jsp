<%-- 
    Document   : newjsp
    Created on : 2022年10月25日, 下午2:22:46
    Author     : miko
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            if (Math.random() < 0.5) {
        %>
        <h1>I am a student.</h1>
        <%
            }else{
        %>
        <h1>I am a teacher.</h1>
        <%
            };
        %>
        
        <table>
        <%
            Date d = new Date();
            for(int i = 0; i < 10; i ++){
        %>
        <tr>
            <td>
                <%= d %>
            </td>
        </tr>
        <%
            };
        %>
        </table>
    </body>
</html>
